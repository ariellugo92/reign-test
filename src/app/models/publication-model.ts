export class PublicationModel {

  author: string;
  objectID?: string;
  story_title?: string;
  story_url?: string;
  created_at: Date;
  isLike?: boolean;

  constructor(author: string, objectID: string, story_title: string, story_url: string, created_at: Date, isLike: boolean) {
    this.author = author;
    this.objectID = objectID;
    this.story_title = story_title;
    this.story_url = story_url;
    this.created_at = created_at;
    this.isLike = isLike;
  }
}
