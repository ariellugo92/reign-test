import { PublicationModel } from "./publication-model";

export interface PaginateResponse {
  hits: PublicationModel[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
}
