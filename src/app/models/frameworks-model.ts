export interface FrameworkModel {
  urlImage: string;
  name: string;
  queryName: 'angular' | 'reactjs' | 'vuejs' | 'none';
}
