import { Component, OnInit } from '@angular/core';
import { DataUserApp } from 'src/app/interfaces/data-user-app';
import { DropdownInterface } from 'src/app/interfaces/dropdown-interface';
import { FrameworkModel } from 'src/app/models/frameworks-model';
import { PublicationModel } from 'src/app/models/publication-model';
import { PublicationService } from 'src/app/services/publication.service';
import { LocalStorageUtils, NAME_DATA_USER } from 'src/app/utils/local-storage-utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  options: DropdownInterface<FrameworkModel>[];
  optionSelected?: FrameworkModel;
  publications?: PublicationModel[];
  loading: boolean = false;
  currentPage: number = 0;
  countItems?: number;
  tabSelected?: number;

  constructor(private publicationService: PublicationService) {
    this.options = [
      { label: 'Angular', value: { urlImage: 'assets/frameworks/angular.png', name: 'angular', queryName: 'angular' } },
      { label: 'React', value: { urlImage: 'assets/frameworks/react.png', name: 'react', queryName: 'reactjs' } },
      { label: 'Vue', value: { urlImage: 'assets/frameworks/vue.png', name: 'vue', queryName: 'vuejs' } },
    ];
  }

  ngOnInit(): void {
    const dataUserApp: DataUserApp = LocalStorageUtils.getLocalStorage(NAME_DATA_USER, true);

    if (dataUserApp) {
      if (dataUserApp.filterSelected != 'none') {
        this.optionSelected = this.options.find(option => option.value.queryName == dataUserApp.filterSelected)?.value;
        this.loadDataUserApp();
      }
      return;
    }

    const newDataUserApp: DataUserApp = {
      filterSelected: 'none',
      objectIDLikes: [],
      tabSelected: 0,
    };

    LocalStorageUtils.setNewLocalStorage({
      key: NAME_DATA_USER,
      isObject: true,
      value: newDataUserApp,
    });
    console.log('entrando aqui');
  }

  loadDataUserApp(clearList: boolean = true): void {
    const dataUserApp: DataUserApp = LocalStorageUtils.getLocalStorage(NAME_DATA_USER, true);
    this.tabSelected = dataUserApp.tabSelected;
    this.loadPublications(dataUserApp, clearList);
  }

  private loadPublications(datauserApp: DataUserApp, clearList: boolean): void {
    this.loading = true;

    if (clearList) {
      this.currentPage = 0;
      this.publications = [];
    }

    this.publicationService.getPublicationsByFw({
      queryName: this.optionSelected?.queryName ?? 'none',
      page: this.currentPage,
    }).subscribe(res => {
      this.currentPage++;
      this.countItems = res.nbHits;
      const objectIDLikes = datauserApp.objectIDLikes;
      const publicationsResponse = res.hits
        .filter(publication => publication.author && publication.story_title && publication.story_url)
        .map(value => ({...value, isLike: !!(objectIDLikes?.find(id => id == value.objectID))})
      );

      if (datauserApp.tabSelected == 1) {
        if (objectIDLikes != null && objectIDLikes.length > 0) {
          this.publications = publicationsResponse.filter(publication => objectIDLikes.findIndex(id => id == publication.objectID) >= 0);
        }

        this.loading = false;
        return;
      }

      this.publications = [...this.publications!, ...publicationsResponse];
      this.loading = false;
    });
  }

  optionSelectedChange(): void {
    const dataUserApp: DataUserApp = LocalStorageUtils.getLocalStorage(NAME_DATA_USER, true);
    const newValue: DataUserApp = { ...dataUserApp, filterSelected: this.optionSelected?.queryName };

    LocalStorageUtils.setNewLocalStorage({
      key: NAME_DATA_USER,
      isObject: true,
      value: newValue,
      cb: () => {
        this.loadDataUserApp();
      },
    });
  }

  onScroll() {
    if ((this.publications && this.countItems) && this.publications?.length <= this.countItems) {
      this.loadDataUserApp(false);
    }
  }

}
