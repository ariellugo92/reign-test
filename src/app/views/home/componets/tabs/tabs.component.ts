import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataUserApp } from 'src/app/interfaces/data-user-app';
import { LocalStorageUtils, NAME_DATA_USER } from 'src/app/utils/local-storage-utils';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  tabIndex: number = 0;

  @Output() changeTabIndex: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
    const dataUserApp: DataUserApp = LocalStorageUtils.getLocalStorage(NAME_DATA_USER, true);
    this.tabIndex = dataUserApp?.tabSelected ?? 0;
  }

  changeTab(index: 0 | 1): void {
    if (index == this.tabIndex) {
      return;
    }

    const dataUserApp: DataUserApp = LocalStorageUtils.getLocalStorage(NAME_DATA_USER, true);
    const newValue: DataUserApp = {...dataUserApp, tabSelected: index};
    console.log(newValue);
    this.tabIndex = index;
    LocalStorageUtils.setNewLocalStorage({
      key: NAME_DATA_USER,
      isObject: true,
      value: newValue,
      cb: () => {
        this.changeTabIndex.emit();
      },
    })
  }

}
