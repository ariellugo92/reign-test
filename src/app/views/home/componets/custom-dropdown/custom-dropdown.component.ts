import { ChangeDetectionStrategy, Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { DropdownInterface } from 'src/app/interfaces/dropdown-interface';

@Component({
  selector: 'app-custom-dropdown',
  templateUrl: './custom-dropdown.component.html',
  styleUrls: ['./custom-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomDropdownComponent<T> implements OnInit {

  @ContentChild(TemplateRef)
  tmpRef!: TemplateRef<T>;

  faAngleDown = faAngleDown;
  isVisible = false;
  hintText = 'Select your news';

  @Input() options: DropdownInterface<T>[] = [];
  @Input() optionSelected?: T;

  @Output() optionSelectedChange = new EventEmitter<T>();

  constructor() { }

  ngOnInit(): void {
    this.getHintText();
  }

  getHintText(): void {
    if (this.optionSelected && this.options?.length > 0) {
      const labelOptionSelected = this.options.find(option => option.value == this.optionSelected)?.label;
      this.hintText = labelOptionSelected ?? this.hintText;
    }
  }

  toggle(): void {
    this.isVisible = !this.isVisible;
  }

  fnTrackBy(index: any, item: DropdownInterface<T>): DropdownInterface<T> {
    return item;
  }

  onChanged(value: T): void {
    this.optionSelected = value;
    this.getHintText();
    this.isVisible = !this.isVisible;
    this.optionSelectedChange.emit(this.optionSelected);
  }

}
