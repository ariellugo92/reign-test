import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import { faHeart as heartRegular } from '@fortawesome/free-regular-svg-icons';
import { faHeart as heartSolid } from '@fortawesome/free-solid-svg-icons';
import { DataUserApp } from 'src/app/interfaces/data-user-app';
import { PublicationModel } from 'src/app/models/publication-model';
import { LocalStorageUtils, NAME_DATA_USER } from 'src/app/utils/local-storage-utils';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  @Input() publication?: PublicationModel;

  @Output() tapLike: EventEmitter<any> = new EventEmitter<any>();

  faClock = faClock;
  heartRegular = heartRegular;
  heartSolid = heartSolid;

  constructor() { }

  ngOnInit(): void {
  }

  openPublication(url?: string): void {
    if (url) {
      window.open(url, "_blank");
    }
  }

  markFavorite(event: any, objectID?: string): void {
    event.stopPropagation();

    if (objectID == null) {
      return;
    }

    const dataUserApp: DataUserApp = LocalStorageUtils.getLocalStorage(NAME_DATA_USER, true);
    const objectIDLikes = dataUserApp.objectIDLikes ?? [];

    const indexId = objectIDLikes?.findIndex(id => objectID == id) ?? -1;
    indexId >= 0 ? objectIDLikes?.splice(indexId, 1) : objectIDLikes?.push(objectID);

    const newValue: DataUserApp = { ...dataUserApp, objectIDLikes: objectIDLikes };

    LocalStorageUtils.setNewLocalStorage({
      key: NAME_DATA_USER,
      isObject: true,
      value: newValue,
      cb: () => {
        this.tapLike.emit();
      },
    });
  }

}
