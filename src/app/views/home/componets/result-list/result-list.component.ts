import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FrameworkModel } from 'src/app/models/frameworks-model';
import { PublicationModel } from 'src/app/models/publication-model';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.scss'],
})
export class ResultListComponent implements OnInit {

  @Input() optionSelected?: FrameworkModel;
  @Input() publications?: PublicationModel[];

  @Output() tapLike: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void { }

  fnTrackBy(index: any, item?: PublicationModel): string | undefined {
    return item?.objectID;
  }

}
