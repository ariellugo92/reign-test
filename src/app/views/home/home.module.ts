import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CustomDropdownComponent } from './componets/custom-dropdown/custom-dropdown.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TabsComponent } from './componets/tabs/tabs.component';
import { ResultListComponent } from './componets/result-list/result-list.component';
import { ItemListComponent } from './componets/result-list/componets/item-list/item-list.component'
import { DateHumanFriendlyPipeModule } from 'src/app/pipes/date-human-friendly.module';
import { OnVisibleDirective } from 'src/app/utils/directives/on-visible.directive';

@NgModule({
  declarations: [
    HomeComponent,
    CustomDropdownComponent,
    TabsComponent,
    ResultListComponent,
    ItemListComponent,
    OnVisibleDirective,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FontAwesomeModule,
    DateHumanFriendlyPipeModule,
  ]
})
export class HomeModule { }
