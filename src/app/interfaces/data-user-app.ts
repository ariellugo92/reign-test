export interface DataUserApp {
  filterSelected?: 'angular' | 'reactjs' | 'vuejs' | 'none';
  objectIDLikes?: string[];
  tabSelected?: 0 | 1;
}
