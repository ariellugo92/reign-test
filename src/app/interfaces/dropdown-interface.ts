export interface DropdownInterface<T> {
  label: string;
  value: T;
}
