import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateHumanFriendlyPipe } from './date-human-friendly.pipe';



@NgModule({
  declarations: [
    DateHumanFriendlyPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DateHumanFriendlyPipe,
  ]
})
export class DateHumanFriendlyPipeModule { }
