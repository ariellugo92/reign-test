import { Pipe, PipeTransform } from '@angular/core';
import { DateUtils } from '../utils/date-utils';

@Pipe({
  name: 'dateHumanFriendly'
})
export class DateHumanFriendlyPipe implements PipeTransform {

  transform(value?: Date): unknown {
    return DateUtils.getDateHumanFriendly(value ?? new Date);
  }

}
