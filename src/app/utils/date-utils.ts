export class DateUtils {

  static getDateHumanFriendly(date: any): string {
    const dateTime = new Date(date);
    const now = new Date();
    let diff = now.getTime() - dateTime.getTime();
    diff /= 1000; // Seconds
    if (diff < 60) {
      return diff.toFixed(0) + ' seconds ago';
    }
    diff /= 60; // Minutes
    if (diff < 60) {
      return diff.toFixed(0) + ' minutes ago';
    }
    diff /= 60; // Hours
    if (diff < 24) {
      return diff.toFixed(0) + ' hours ago';
    }
    return dateTime.toLocaleString();
  }

}
