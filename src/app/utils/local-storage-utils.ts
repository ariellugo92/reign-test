export const NAME_DATA_USER = 'data-user-app';

export class LocalStorageUtils {

  static setNewLocalStorage(data: {key: string, value: any, isObject: boolean, cb?: Function}): void {
    const dataLocalStorage: string = data.isObject ? JSON.stringify(data.value) : data.value;
    localStorage.setItem(data.key, dataLocalStorage);

    if (data.cb) {
      data.cb();
    }
  }

  static getLocalStorage(key: string, isObject: boolean): any {
    const dataLocalStorage = localStorage.getItem(key);
    if (isObject && dataLocalStorage) {
      return JSON.parse(dataLocalStorage);
    }

    return dataLocalStorage;
  }

  static removeLocalStorage(key: string, cb?: Function): void {
    localStorage.removeItem(key);

    if (cb) {
      cb();
    }
  }

}
