import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaginateResponse } from '../models/paginate-response';

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  constructor(private http: HttpClient) { }

  getPublicationsByFw({queryName, page}: {queryName: 'angular' | 'reactjs' | 'vuejs' | 'none', page: number}): Observable<PaginateResponse> {
    const url = `https://hn.algolia.com/api/v1/search_by_date?query=${queryName}&page=${page}`;

    return this.http.get<PaginateResponse>(url);
  }
}
