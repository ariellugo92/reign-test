# Prueba para el puesto de desarrollor web - REIGN
Proyecto desarrollado en angular 13.

## Funcionalidades.

- Dropdown para la seleccioón del framework/librería a utilizar. 
- Listado de publicaciones.
- Listado de publicaciones a las que he reaccionado.
- Reacción a las publicaciones.
- Consumo de servicios REST

## Proceso para ejecutar el proyecto.

```sh
git clone https://gitlab.com/ariellugo92/reign-test.git
cd reign-test
npm i
ng serve
```

## Entorno de desarrollo.

- [Angular](https://angular.io/) - 13.3.0
- [Node](https://nodejs.org/en/) - 16.14.0
- [Angular CLI](https://angular.io/cli) - 13.3.11.

## Paquetes de terceros.

| Paquete |  |
| ------ | ------ |
| fortawesome@0.10.0 | Paquete de iconos. |
| bootstrap@5.2.0 | UI/CSS |

##### Scroll Infinito

La implementacioón de la directiva para el scroll infino se basa en lo explicado en [este enlace](https://www.algolia.com/doc/guides/building-search-ui/ui-and-ux-patterns/infinite-scroll/angular/)
